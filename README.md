# Restaurant POS SERVICE

This project is powered by Ruby on rails, doorkeeper, oauth2.

# Installation Process

**Unzip the folder** and open terminal and write: 

**$ cd restaurant_service**

Install bundler : **$ gem install bundler --pre**

Install college : **$ bundle install**

# Database Setup

Open `config-> database.yml` and **change in your Mysql database username and password**

**Database creation :** $ `bundle exec rake db:create`

**Database migrate :** $ `bundle exec rake db:migrate`

**Database seed :** $ `bundle exec rake db:seed`

# Run server
open terminal and run
$ `rails s`

# How it Works ?

Restaurant POS Service is a secured REST API system for a small restaurant.

# How to manage credentials ?
Run server using `$ rails s` and you have to create new oauth credentials for using this service. 

Open browser and go to `http://localhost:3000/oauth/applications/new` and create a new application.

You will get a `Application ID` and `Secret` and  a `callback url`.

Now open **POSTMAN** or **INSOMNIA** and fill up the `oauth 2` credential fields as like:

**Grant Type** : `Client Credentials`

**Access Token Url** : `http://localhost:3000/oauth/token`

**Client ID** : `Your application id which has been created a few times ago`

**Client Secret** : `Your application secret which has been created a few times ago`

Now click the button of `Refresh button` and get your token.


## Features

**Category feature**

Category feature contains name and description of a product types.
It tells us product is food or drink type. It contains `name` and `description` attributes.
Category has many products.


**Creation:** : `POST http://localhost:3000/api/categories`

```
{
	"name": "dessert",
	"description": "all types dessert"
	
}

```

**Read**

`GET http://localhost:3000/api/categories`

`GET http://localhost:3000/api/categories/{id}`

**Update**
`PUT/PATCH http://localhost:3000/api/categories/{id}`

**Delete**
`DELETE http://localhost:3000/api/categories/{id}`


**Customer feature**

Customer feature contains data of restaurant customers. It contains `name`, `address`, `birthday`, `email` and
`phone` attributes. Customer has many orders.

**Creation:** : `POST http://localhost:3000/api/customers`

```
	{
      "name": "Sheri Barrows",
  		"address": "03563 Rosalba Crescent",
  		"email": "claretha@graham.com",
  		"birthday": "1988-04-01",
  		"phone": "(483)421-6617"
  }
	
```

**Read**

`GET http://localhost:3000/api/customers`

`GET http://localhost:3000/api/customers/{id}`

**Update**
`PUT/PATCH http://localhost:3000/api/customers/{id}`

**Delete**
`DELETE http://localhost:3000/api/customers/{id}`



**Product feature**

Product feature contains data of restaurant product that means foods, drinks etc.
It contains `name`, `price`, `description`, `category` attributes. Product belongs to category.

**Creation:** : `POST http://localhost:3000/api/products`

```
{
	  "name": "Chicken Burger",
		"price": 10,
		"description": "Spicy Chicken burger",
		"category_id": 1
}
	
```

**Read**

`GET http://localhost:3000/api/products`

`GET http://localhost:3000/api/products/{id}`

**Update**
`PUT/PATCH http://localhost:3000/api/products/{id}`

**Delete**
`DELETE http://localhost:3000/api/products/{id}`



**Waiter feature**

Waiter feature contains data of restaurant waiters. It contains `name`, `address` and `phone` attributes.
Waiter has many orders.

**Creation:** : `POST http://localhost:3000/api/waiters`

```
{
	  "name": "Clare Zulauf1",
		"address": "00903 Torphy Isle",
		"phone": "1-801-858-2446"
}
	
```

**Read**

`GET http://localhost:3000/api/waiters`

`GET http://localhost:3000/api/waiters/{id}`

**Update**
`PUT/PATCH http://localhost:3000/api/waiters/{id}`

**Delete**
`DELETE http://localhost:3000/api/waiters/{id}`


**Table feature**

Table feature contains data of restaurant tables. It contains `name`, `capacity` and `indoor` attributes.
Table has many orders.

**Creation:** : `POST http://localhost:3000/api/tables`

```
{
	    "name": "East Table",
  		"capacity": 6,
  		"indoor": true
}
	
```

**Read**

`GET http://localhost:3000/api/tables`

`GET http://localhost:3000/api/tables/{id}`

**Update**
`PUT/PATCH http://localhost:3000/api/tables/{id}`

**Delete**
`DELETE http://localhost:3000/api/tables/{id}`


**Order feature**

Order feature contains data of restaurant orders. It contains `discount`, `brute`, `net`, `payed` , `payed at`
attributes. Order has many products and tables.

**Creation:** : `POST http://localhost:3000/api/orders`

```
{
    "discount": 10,
   	"brute": 230,
   	"net": 227,
   	"payed": true,
   	"payed_at": "2017-10-12T14:17:50.000Z",
   	"customer_id": 1,
   	"waiter_id": 1,
   	"product_ids": [1,3,5],
   	"table_ids": [1]
}
	
```

**Read**

`GET http://localhost:3000/api/orders`

`GET http://localhost:3000/api/orders/{id}`

**Update**
`PUT/PATCH http://localhost:3000/api/orders/{id}`

**Delete**
`DELETE http://localhost:3000/api/orders/{id}`


# Tools used
Ruby (version 2.4.1)

Ruby on Rails(version 5.1)

Mysql

Active Model Serialization

Doorkeeper

Auth2

RSpec, FactoryGirl

# Testing

Run all testing: `bundle exec rspec spec`

Testing coverage is 100%
