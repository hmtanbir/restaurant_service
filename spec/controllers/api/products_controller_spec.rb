require 'rails_helper'

RSpec.describe Api::ProductsController, type: :controller do

  let(:token) { double acceptable?: true }
  before(:each) do
    allow(controller).to receive(:doorkeeper_token) { token }
  end
  # This should return the minimal set of attributes required to create a valid
  # Product. As you add validations to Product, be sure to
  # adjust the attributes here as well.
  let(:category) { FactoryGirl.create :category }
  let(:valid_attributes) {
    FactoryGirl.attributes_for :product, category_id: category.id
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for :product, name: nil
  }
  
  describe "GET #index" do
    it "returns a success response" do
      product = Product.create! valid_attributes
      get :index, params: {}
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    context 'with valid params' do
      it "returns a success response" do
        product = Product.create! valid_attributes
        get :show, params: {id: product.to_param}
        expect(response).to be_success
      end
    end

    context 'with invalid params' do
      it 'returns null' do
        get :show, params: { id: 100 }
        expect(response.body).to eq('null')
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Product" do
        expect {
          post :create, params: {product: valid_attributes}
        }.to change(Product, :count).by(1)
      end

      it "renders a JSON response with the new product" do

        post :create, params: {product: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(api_product_url(Product.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new product" do

        post :create, params: {product: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:product, name: 'Chicken Burger', category_id: category.id)
      }

      it "updates the requested product" do
        product = Product.create! valid_attributes
        put :update, params: {id: product.to_param, product: new_attributes}
        product.reload
        expect(Product.first.name).to eq('Chicken Burger')
      end

      it "renders a JSON response with the product" do
        product = Product.create! valid_attributes

        put :update, params: {id: product.to_param, product: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the product" do
        product = Product.create! valid_attributes

        put :update, params: {id: product.to_param, product: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested product" do
      product = Product.create! valid_attributes
      expect {
        delete :destroy, params: {id: product.to_param}
      }.to change(Product, :count).by(-1)
    end
  end

end
