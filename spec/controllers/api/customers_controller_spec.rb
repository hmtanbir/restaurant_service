require 'rails_helper'

RSpec.describe Api::CustomersController, type: :controller do

  let(:token) { double acceptable?: true }
  before(:each) do
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  # This should return the minimal set of attributes required to create a valid
  # Customer. As you add validations to Customer, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:customer)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:customer, name: nil)
  }
    
  describe "GET #index" do
    it "returns a success response" do
      customer = Customer.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    context 'with valid params' do
      it "returns a success response" do
        customer = Customer.create! valid_attributes
        get :show, params: {id: customer.to_param}
        expect(response).to be_success
      end
    end

    context 'with invalid params' do
      it 'returns null' do
        get :show, params: { id: 100 }
        expect(response.body).to eq('null')
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Customer" do
        expect {
          post :create, params: {customer: valid_attributes}
        }.to change(Customer, :count).by(1)
      end

      it "renders a JSON response with the new customer" do

        post :create, params: {customer: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(api_customer_url(Customer.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new customer" do

        post :create, params: {customer: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:customer, name: 'Mark Doe')
      }

      it "updates the requested customer" do
        customer = Customer.create! valid_attributes
        put :update, params: {id: customer.to_param, customer: new_attributes}
        customer.reload
        expect(Customer.first.name).to eq('Mark Doe')
      end

      it "renders a JSON response with the customer" do
        customer = Customer.create! valid_attributes

        put :update, params: {id: customer.to_param, customer: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the customer" do
        customer = Customer.create! valid_attributes

        put :update, params: {id: customer.to_param, customer: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested customer" do
      customer = Customer.create! valid_attributes
      expect {
        delete :destroy, params: {id: customer.to_param}
      }.to change(Customer, :count).by(-1)
    end
  end

end
