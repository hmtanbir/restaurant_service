require 'rails_helper'

RSpec.describe Api::WaitersController, type: :controller do

  let(:token) { double acceptable?: true }
  before(:each) do
    allow(controller).to receive(:doorkeeper_token) { token }
  end
  # This should return the minimal set of attributes required to create a valid
  # Waiter. As you add validations to Waiter, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:waiter)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:waiter, name: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      waiter = Waiter.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    context 'with valid params' do
      it "returns a success response" do
        waiter = Waiter.create! valid_attributes
        get :show, params: {id: waiter.to_param}
        expect(response).to be_success
      end
    end

    context 'with invalid params' do
      it 'returns null' do
        get :show, params: { id: 100 }
        expect(response.body).to eq('null')
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Waiter" do
        expect {
          post :create, params: {waiter: valid_attributes}
        }.to change(Waiter, :count).by(1)
      end

      it "renders a JSON response with the new waiter" do

        post :create, params: {waiter: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(api_waiter_url(Waiter.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new waiter" do

        post :create, params: {waiter: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:waiter, name: 'Adam Jumpa')
      }

      it "updates the requested waiter" do
        waiter = Waiter.create! valid_attributes
        put :update, params: {id: waiter.to_param, waiter: new_attributes}
        waiter.reload
        expect(Waiter.first.name).to eq('Adam Jumpa')
      end

      it "renders a JSON response with the waiter" do
        waiter = Waiter.create! valid_attributes

        put :update, params: {id: waiter.to_param, waiter: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the waiter" do
        waiter = Waiter.create! valid_attributes

        put :update, params: {id: waiter.to_param, waiter: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested waiter" do
      waiter = Waiter.create! valid_attributes
      expect {
        delete :destroy, params: {id: waiter.to_param}
      }.to change(Waiter, :count).by(-1)
    end
  end

end
