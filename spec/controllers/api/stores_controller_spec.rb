require 'rails_helper'

RSpec.describe Api::StoresController, type: :controller do

  let(:token) { double acceptable?: true }
  before(:each) do
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  # This should return the minimal set of attributes required to create a valid
  # Store. As you add validations to Store, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:store)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:store, name: nil, phone: nil)
  }
  
  describe "GET #index" do
    it "returns a success response" do
      store = Store.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    context 'with valid params' do
      it "returns a success response" do
        store = Store.create! valid_attributes
        get :show, params: {id: store.to_param}
        expect(response).to be_success
      end
    end

    context 'with invalid params' do
      it 'returns null' do
        get :show, params: { id: 100 }
        expect(response.body).to eq('null')
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Store" do
        expect {
          post :create, params: {store: valid_attributes}
        }.to change(Store, :count).by(1)
      end

      it "renders a JSON response with the new store" do

        post :create, params: {store: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(api_store_url(Store.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new store" do

        post :create, params: {store: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:store, name: 'New Store')
      }

      it "updates the requested store" do
        store = Store.create! valid_attributes
        put :update, params: {id: store.to_param, store: new_attributes}
        store.reload
        expect(Store.first.name).to eq('New Store')
      end

      it "renders a JSON response with the store" do
        store = Store.create! valid_attributes

        put :update, params: {id: store.to_param, store: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the store" do
        store = Store.create! valid_attributes

        put :update, params: {id: store.to_param, store: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested store" do
      store = Store.create! valid_attributes
      expect {
        delete :destroy, params: {id: store.to_param}
      }.to change(Store, :count).by(-1)
    end
  end

end
