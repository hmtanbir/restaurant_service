require 'rails_helper'

RSpec.describe Api::OrdersController, type: :controller do

  let(:token) { double acceptable?: true }
  before(:each) do
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  # This should return the minimal set of attributes required to create a valid
  # Order. As you add validations to Order, be sure to
  # adjust the attributes here as well.
  let(:category) { FactoryGirl.create :category }
  let(:product) { FactoryGirl.create :product, category_id: category.id }
  let(:table) { FactoryGirl.create :table }
  let(:customer) { FactoryGirl.create :customer }
  let(:waiter) { FactoryGirl.create :waiter }
  let(:valid_attributes) {
    FactoryGirl.attributes_for :order,
                               customer_id: customer.id,
                               waiter_id: waiter.id,
                               product_ids: [product.id],
                               table_ids: [table.id]
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for :order, net: nil
  }

  describe "GET #index" do
    it "returns a success response" do
      order = Order.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    context 'with valid params' do
      it "returns a success response" do
        order = Order.create! valid_attributes
        get :show, params: {id: order.to_param}
        expect(response).to be_success
      end
    end

    context 'with invalid params' do
      it 'returns null' do
        get :show, params: { id: 100 }
        expect(response.body).to eq('null')
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Order" do
        expect {
          post :create, params: {order: valid_attributes}
        }.to change(Order, :count).by(1)
      end

      it "renders a JSON response with the new order" do

        post :create, params: {order: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(api_order_url(Order.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new order" do

        post :create, params: {order: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for :order, net: 50, customer_id: customer.id, waiter_id: waiter.id
      }

      it "updates the requested order" do
        order = Order.create! valid_attributes
        put :update, params: {id: order.to_param, order: new_attributes}
        order.reload
        expect(Order.first.net).to eq(50)
      end

      it "renders a JSON response with the order" do
        order = Order.create! valid_attributes

        put :update, params: {id: order.to_param, order: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the order" do
        order = Order.create! valid_attributes

        put :update, params: {id: order.to_param, order: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested order" do
      order = Order.create! valid_attributes
      expect {
        delete :destroy, params: {id: order.to_param}
      }.to change(Order, :count).by(-1)
    end
  end

end
