require 'rails_helper'

RSpec.describe Api::CategoriesController, type: :controller do

  let(:token) { double acceptable?: true }
  before(:each) do
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  # This should return the minimal set of attributes required to create a valid
  # Category. As you add validations to Category, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:category)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:category, name: nil)
  }
  
  describe "GET #index" do

    it "returns a success response" do
      category = Category.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    context 'valid params' do
      it "returns a success response" do
        category = Category.create! valid_attributes
        get :show, params: {id: category.to_param}
        expect(response).to be_success
      end
    end

    context 'with invalid params' do
      it 'returns null' do
        get :show, params: { id: 100 }
        expect(response.body).to eq('null')
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Category" do
        expect {
          post :create, params: {category: valid_attributes}
        }.to change(Category, :count).by(1)
      end

      it "renders a JSON response with the new category" do

        post :create, params: {category: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(api_category_url(Category.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new category" do

        post :create, params: {category: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:category, name: 'Drink')
      }

      it "updates the requested category" do
        category = Category.create! valid_attributes
        put :update, params: {id: category.to_param, category: new_attributes}
        category.reload
        expect(Category.first.name).to eq('Drink')
      end

      it "renders a JSON response with the category" do
        category = Category.create! valid_attributes

        put :update, params: {id: category.to_param, category: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the category" do
        category = Category.create! valid_attributes

        put :update, params: {id: category.to_param, category: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested category" do
      category = Category.create! valid_attributes
      expect {
        delete :destroy, params: {id: category.to_param}
      }.to change(Category, :count).by(-1)
    end
  end

end
