require 'rails_helper'

RSpec.describe Api::TablesController, type: :controller do

  let(:token) { double acceptable?: true }
  before(:each) do
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  # This should return the minimal set of attributes required to create a valid
  # Table. As you add validations to Table, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:table)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:table, name: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      table = Table.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    context 'with valid params' do
      it "returns a success response" do
        table = Table.create! valid_attributes
        get :show, params: {id: table.to_param}
        expect(response).to be_success
      end
    end

    context 'with invalid params' do
      it 'returns null' do
        get :show, params: { id: 100 }
        expect(response.body).to eq('null')
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Table" do
        expect {
          post :create, params: {table: valid_attributes}
        }.to change(Table, :count).by(1)
      end

      it "renders a JSON response with the new table" do

        post :create, params: {table: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(api_table_url(Table.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new table" do

        post :create, params: {table: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:table, name: 'New table')
      }

      it "updates the requested table" do
        table = Table.create! valid_attributes
        put :update, params: {id: table.to_param, table: new_attributes}
        table.reload
        expect(Table.first.name).to eq('New table')
      end

      it "renders a JSON response with the table" do
        table = Table.create! valid_attributes

        put :update, params: {id: table.to_param, table: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the table" do
        table = Table.create! valid_attributes

        put :update, params: {id: table.to_param, table: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested table" do
      table = Table.create! valid_attributes
      expect {
        delete :destroy, params: {id: table.to_param}
      }.to change(Table, :count).by(-1)
    end
  end

end
