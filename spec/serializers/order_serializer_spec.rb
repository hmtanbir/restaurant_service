require 'rails_helper'

RSpec.describe OrderSerializer, type: :serializer do
  let(:order) { FactoryGirl.build(:order) }
  let(:serializer) { described_class.new(order) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

  let(:subject) { JSON.parse(serialization.to_json) }

  it 'has an id that matches' do
    expect(subject['id']).to eql(order.id)
  end

  it 'has a discount that matches' do
    expect(subject['discount']).to eql(order.discount)
  end

  it 'has a brute that matches' do
    expect(subject['brute']).to eql(order.brute)
  end

  it 'has a net that matches' do
    expect(subject['net']).to eql(order.net)
  end

  it 'has a payed that matches' do
    expect(subject['payed']).to eql(order.payed)
  end
end
