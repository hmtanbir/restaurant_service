require 'rails_helper'

RSpec.describe ProductSerializer, type: :serializer do
  let(:product) { FactoryGirl.build(:product) }
  let(:serializer) { described_class.new(product) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

  let(:subject) { JSON.parse(serialization.to_json) }

  it 'has an id that matches' do
    expect(subject['id']).to eql(product.id)
  end

  it 'has a name that matches' do
    expect(subject['name']).to eql(product.name)
  end

  it 'has a price that matches' do
    expect(subject['price']).to eql(product.price)
  end

  it 'has a description that matches' do
    expect(subject['description']).to eql(product.description)
  end
end
