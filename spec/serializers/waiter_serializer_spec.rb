require 'rails_helper'

RSpec.describe WaiterSerializer, type: :serializer do
  let(:waiter) { FactoryGirl.build(:waiter) }
  let(:serializer) { described_class.new(waiter) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

  let(:subject) { JSON.parse(serialization.to_json) }

  it 'has an id that matches' do
    expect(subject['id']).to eql(waiter.id)
  end

  it 'has a name that matches' do
    expect(subject['name']).to eql(waiter.name)
  end

  it 'has an address that matches' do
    expect(subject['address']).to eql(waiter.address)
  end

  it 'has a phone that matches' do
    expect(subject['phone']).to eql(waiter.phone)
  end
end
