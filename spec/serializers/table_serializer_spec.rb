require 'rails_helper'

RSpec.describe TableSerializer, type: :serializer do
  let(:table) { FactoryGirl.build(:table) }
  let(:serializer) { described_class.new(table) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

  let(:subject) { JSON.parse(serialization.to_json) }

  it 'has an id that matches' do
    expect(subject['id']).to eql(table.id)
  end

  it 'has a name that matches' do
    expect(subject['name']).to eql(table.name)
  end

  it 'has a capacity that matches' do
    expect(subject['capacity']).to eql(table.capacity)
  end

  it 'has an indoor option that matches' do
    expect(subject['indoor']).to eql(table.indoor)
  end
end
