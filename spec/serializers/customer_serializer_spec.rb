require 'rails_helper'

RSpec.describe CustomerSerializer, type: :serializer do
  let(:customer) { FactoryGirl.create(:customer) }
  let(:serializer) { described_class.new(customer) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

  let(:subject) { JSON.parse(serialization.to_json) }

  it 'has an id that matches' do
    expect(subject['id']).to eql(customer.id)
  end

  it 'has a name that matches' do
    expect(subject['name']).to eql(customer.name)
  end

  it 'has a address that matches' do
    expect(subject['address']).to eql(customer.address)
  end

  it 'has an email that matches' do
    expect(subject['email']).to eql(customer.email)
  end

  it 'has a birthday that matches' do
    expect(subject['birthday']).to eql(customer.birthday.strftime('%F'))
  end

  it 'has a phone that matches' do
    expect(subject['phone']).to eql(customer.phone)
  end
end
