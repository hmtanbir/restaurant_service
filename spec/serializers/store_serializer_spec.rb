require 'rails_helper'

RSpec.describe StoreSerializer, type: :serializer do
  let(:store) { FactoryGirl.build(:store) }
  let(:serializer) { described_class.new(store) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

  let(:subject) { JSON.parse(serialization.to_json) }

  it 'has an id that matches' do
    expect(subject['id']).to eql(store.id)
  end

  it 'has a name that matches' do
    expect(subject['name']).to eql(store.name)
  end

  it 'has a description that matches' do
    expect(subject['description']).to eql(store.description)
  end

  it 'has a phone that matches' do
    expect(subject['phone']).to eql(store.phone)
  end

  it 'has a address that matches' do
    expect(subject['address']).to eql(store.address)
  end

  it 'has a tax_rate that matches' do
    expect(subject['tax_rate']).to eql(store.tax_rate)
  end

  it 'has an website that matches' do
    expect(subject['website']).to eql(store.website)
  end
end
