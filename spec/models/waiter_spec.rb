require 'rails_helper'

RSpec.describe Waiter, type: :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :address }
  it { is_expected.to validate_presence_of :phone }
  it { is_expected.to validate_uniqueness_of :phone }
end
