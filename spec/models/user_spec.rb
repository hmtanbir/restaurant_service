require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to define_enum_for(:role).with([:staff, :admin]) }
  it { is_expected.to validate_presence_of :name }

  describe '.set_default_role' do
    let(:user) { FactoryGirl.create :user }
    it 'returns staff role' do
      expect(user.set_default_role).to eq('staff')
    end
  end
end
