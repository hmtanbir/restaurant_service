require 'rails_helper'

RSpec.describe Order, type: :model do
  it { is_expected.to validate_presence_of :brute }
  it { is_expected.to validate_presence_of :net }
  it { is_expected.to belong_to :customer }
  it { is_expected.to belong_to :waiter }
  it { is_expected.to have_and_belong_to_many :products }
  it { is_expected.to have_and_belong_to_many :tables }
end
