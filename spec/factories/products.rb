FactoryGirl.define do
  factory :product do
    name 'Beef Burger'
    price '9.99'
    description 'Hot and spicy beef burger'
    category nil
  end
end
