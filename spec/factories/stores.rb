FactoryGirl.define do
  factory :store do
    name 'Restaurant Service'
    description 'Rest Secured API System for Restaurant'
    phone '1223454'
    address 'Washington'
    tax_rate '9.99'
    website 'http://example.com'
  end
end
