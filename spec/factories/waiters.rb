FactoryGirl.define do
  factory :waiter do
    name 'Adam Smith'
    address 'Sydney, Australia'
    phone '2345677654'
  end
end
