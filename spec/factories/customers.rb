FactoryGirl.define do
  factory :customer do
    name 'John Doe'
    address 'Washington D.C'
    email 'john@example.com'
    birthday '1991-10-09'
    phone '12323344'
  end
end
