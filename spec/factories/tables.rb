FactoryGirl.define do
  factory :table do
    name 'South Table'
    capacity 10
    indoor false
  end
end
