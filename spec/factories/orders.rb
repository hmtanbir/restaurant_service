FactoryGirl.define do
  factory :order do
    discount '9.99'
    brute '19.99'
    net '29.99'
    payed false
    payed_at '2017-10-07 21:46:08'
    customer nil
    waiter nil
  end
end
