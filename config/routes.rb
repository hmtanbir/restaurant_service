Rails.application.routes.draw do
  use_doorkeeper
  namespace :api do
    resources :orders
    resources :products
    resources :customers
    resources :tables
    resources :waiters
    resources :stores
    resources :categories
  end
  # root path
  root to: 'home#index'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
