class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.decimal :discount
      t.decimal :brute
      t.decimal :net
      t.boolean :payed, default: false
      t.datetime :payed_at
      t.belongs_to :customer, foreign_key: true
      t.belongs_to :waiter, foreign_key: true

      t.timestamps
    end
  end
end
