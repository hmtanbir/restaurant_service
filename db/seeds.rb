# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(name: 'Admin User', email: 'admin@example.com', password: 'secret', role: 'admin')

# Seed for Store
Store.create(name: 'Best Restaurant',
             description: 'It is a best restaurant',
             tax_rate: 10,
             phone: '12346542',
             address: 'New York',
             website: 'example.com')

# Seed for Category
category_1= Category.create(name: 'Food', description: 'all types food')
category_2= Category.create(name: 'Drink', description: 'all types drink')

# Seed for products
product_1 = Product.create(name: 'Beef Burger', price: 10, description: 'Spicy Beef burger', category_id: category_1.id)
product_2 = Product.create(name: 'Pop Chicken', price: 40, description: 'Spicy Pop Chicken', category_id: category_1.id)
product_3 = Product.create(name: 'Fried Chicken Ball', price: 100, description: 'Fried Chicken Ball', category_id: category_1.id)
product_4 = Product.create(name: 'Thai Soup', price: 20, description: 'Thai Soup', category_id: category_1.id)
product_5 = Product.create(name: 'Chicken Vegetable Soup', price: 30, description: 'Chicken Vegetable Soup', category_id: category_1.id)
product_6 = Product.create(name: 'Mineral Water', price: 10, description: 'Cold Mineral Water', category_id: category_2.id)
product_7 = Product.create(name: 'Pepsi', price: 20, description: 'Cold Pepsi', category_id: category_2.id)

# Seed for Customer
customer_1 = Customer.create(name: FFaker::Name.name, address: FFaker::Address.street_address, email: FFaker::Internet.email, birthday: '1988-04-01', phone: FFaker::PhoneNumber.phone_number)
customer_2 = Customer.create(name: FFaker::Name.name, address: FFaker::Address.street_address, email: FFaker::Internet.email, birthday: '1988-03-01', phone: FFaker::PhoneNumber.phone_number)
customer_3 = Customer.create(name: FFaker::Name.name, address: FFaker::Address.street_address, email: FFaker::Internet.email, birthday: '1988-04-01', phone: FFaker::PhoneNumber.phone_number)

# Seed for Waiter
waiter_1 = Waiter.create(name: FFaker::Name.name, address: FFaker::Address.street_address, phone: FFaker::PhoneNumber.phone_number)
waiter_2 = Waiter.create(name: FFaker::Name.name, address: FFaker::Address.street_address, phone: FFaker::PhoneNumber.phone_number)
waiter_3 = Waiter.create(name: FFaker::Name.name, address: FFaker::Address.street_address, phone: FFaker::PhoneNumber.phone_number)

# Seed for restaurant Table
table_1 = Table.create(name: 'North Table', capacity: 6, indoor: false)
table_2 = Table.create(name: 'South Table', capacity: 6, indoor: true)

# Seed for Order
order_1 = Order.create(discount: 10, brute: 230, net: 227, payed: true, payed_at: Time.now(), customer_id: customer_1.id, waiter_id: waiter_1.id)
order_1.products << [product_1, product_2, product_3, product_4, product_5, product_6, product_7]
order_1.tables << [table_1]

order_1 = Order.create(discount: 0, brute: 160, net: 176, payed: true, payed_at: Time.now(), customer_id: customer_2.id, waiter_id: waiter_2.id)
order_1.products << [product_1, product_2, product_3, product_6]
order_1.tables << [table_2]

order_1 = Order.create(discount: 25, brute: 200, net: 165, payed: false, customer_id: customer_3.id, waiter_id: waiter_3.id)
order_1.products << [product_1, product_2, product_3, product_4, product_6, product_7]
order_1.tables << [table_1]

puts 'Database seeding successfully.'
