class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :email, :birthday, :phone
end
