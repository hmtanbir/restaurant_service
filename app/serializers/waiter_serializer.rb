class WaiterSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :phone
end
