class StoreSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :phone, :address, :tax_rate, :website
end
