class OrderSerializer < ActiveModel::Serializer
  attributes :id, :discount, :brute, :net, :payed, :payed_at
  has_one :customer
  has_one :waiter
  has_many :products
  has_many :tables
end
