class TableSerializer < ActiveModel::Serializer
  attributes :id, :name, :capacity, :indoor
end
