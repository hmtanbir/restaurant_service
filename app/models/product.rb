class Product < ApplicationRecord
  belongs_to :category
  validates_presence_of :name, :price
  validates_uniqueness_of :name
  has_and_belongs_to_many :orders
end
