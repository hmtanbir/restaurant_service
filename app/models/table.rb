class Table < ApplicationRecord
  validates_presence_of :name, :capacity
  validates_uniqueness_of :name
  has_and_belongs_to_many :orders
end
