class Store < ApplicationRecord
  validates_presence_of :name, :phone
  validates_uniqueness_of :name, :phone, :website
end
