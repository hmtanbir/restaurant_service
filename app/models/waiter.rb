class Waiter < ApplicationRecord
  validates_presence_of :name, :address, :phone
  validates_uniqueness_of :phone
end
