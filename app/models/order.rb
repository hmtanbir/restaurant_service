class Order < ApplicationRecord
  attribute :product_ids
  attribute :table_ids
  belongs_to :customer
  belongs_to :waiter
  validates_presence_of :brute, :net
  has_and_belongs_to_many :products, dependent: :destroy
  has_and_belongs_to_many :tables, dependent: :destroy
end
