module Api
  class CategoriesController < ApiController
    before_action :set_category, only: [:show, :update, :destroy]

    # GET /api/categories
    def index
      @categories = Category.all

      render json: @categories
    end

    # GET /api/categories/1
    def show
      render json: @category
    end

    # POST /api/categories
    def create
      @category = Category.new(category_params)

      if @category.save
        render json: @category, status: :created, location: api_category_url(@category)
      else
        render json: @category.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/categories/1
    def update
      if @category.present?
        if @category.update(category_params)
          render json: @category
        else
          render json: @category.errors, status: :unprocessable_entity
        end
      end
    end

    # DELETE /api/categories/1
    def destroy
      @category.destroy if @category.present?
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error{ 'Category record is not found' }
      nil
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit(:name, :description)
    end
  end

end
