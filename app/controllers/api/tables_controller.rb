module Api
  class TablesController < ApiController
    before_action :set_table, only: [:show, :update, :destroy]

    # GET /api/tables
    def index
      @tables = Table.all

      render json: @tables
    end

    # GET /api/tables/1
    def show
      render json: @table
    end

    # POST /api/tables
    def create
      @table = Table.new(table_params)

      if @table.save
        render json: @table, status: :created, location: api_table_url(@table)
      else
        render json: @table.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/tables/1
    def update
      if @table.present?
        if @table.update(table_params)
          render json: @table
        else
          render json: @table.errors, status: :unprocessable_entity
        end
      end
    end

    # DELETE /api/tables/1
    def destroy
      @table.destroy if @table.present?
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_table
      @table = Table.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error{ 'Table record is not found' }
      nil
    end

    # Only allow a trusted parameter "white list" through.
    def table_params
      params.require(:table).permit(:name, :capacity, :indoor)
    end
  end
end
