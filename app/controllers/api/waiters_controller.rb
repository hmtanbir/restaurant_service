module Api
  class WaitersController < ApiController
    before_action :set_waiter, only: [:show, :update, :destroy]

    # GET /api/waiters
    def index
      @waiters = Waiter.all

      render json: @waiters
    end

    # GET /api/waiters/1
    def show
      render json: @waiter
    end

    # POST /api/waiters
    def create
      @waiter = Waiter.new(waiter_params)

      if @waiter.save
        render json: @waiter, status: :created, location: api_waiter_url(@waiter)
      else
        render json: @waiter.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/waiters/1
    def update
      if @waiter.present?
        if @waiter.update(waiter_params)
          render json: @waiter
        else
          render json: @waiter.errors, status: :unprocessable_entity
        end
      end
    end

    # DELETE /api/waiters/1
    def destroy
      @waiter.destroy if @waiter.present?
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_waiter
      @waiter = Waiter.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error{ 'Waiter record is not found' }
      nil
    end

    # Only allow a trusted parameter "white list" through.
    def waiter_params
      params.require(:waiter).permit(:name, :address, :phone)
    end
  end
end