module Api
  class StoresController < ApiController
    before_action :set_store, only: [:show, :update, :destroy]

    # GET /api/stores
    def index
      @stores = Store.all

      render json: @stores
    end

    # GET /api/stores/1
    def show
      render json: @store
    end

    # POST /api/stores
    def create
      @store = Store.new(store_params)

      if @store.save
        render json: @store, status: :created, location: api_store_url(@store)
      else
        render json: @store.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/stores/1
    def update
      if @store.present?
        if @store.update(store_params)
          render json: @store
        else
          render json: @store.errors, status: :unprocessable_entity
        end
      end
    end

    # DELETE /api/stores/1
    def destroy
      @store.destroy if @store.present?
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_store
      @store = Store.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error{ 'Store record is not found' }
      nil
    end

    # Only allow a trusted parameter "white list" through.
    def store_params
      params.require(:store).permit(:name, :description, :phone, :address, :tax_rate, :website)
    end
  end
end
