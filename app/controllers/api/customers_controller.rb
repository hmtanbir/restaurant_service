module Api
  class CustomersController < ApiController
    before_action :set_customer, only: [:show, :update, :destroy]

    # GET /api/customers
    def index
      @customers = Customer.all

      render json: @customers
    end

    # GET /api/customers/1
    def show
      render json: @customer
    end

    # POST /api/customers
    def create
      @customer = Customer.new(customer_params)

      if @customer.save
        render json: @customer, status: :created, location: api_customer_url(@customer)
      else
        render json: @customer.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/customers/1
    def update
      if @customer.present?
        if @customer.update(customer_params)
          render json: @customer
        else
          render json: @customer.errors, status: :unprocessable_entity
        end
      end
    end

    # DELETE /api/customers/1
    def destroy
      @customer.destroy if @customer.present?
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error{ 'Customer record is not found' }
      nil
    end

    # Only allow a trusted parameter "white list" through.
    def customer_params
      params.require(:customer).permit(:name, :address, :email, :birthday, :phone)
    end
  end
end
