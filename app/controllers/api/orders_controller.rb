module Api
  class OrdersController < ApiController
    before_action :set_order, only: [:show, :update, :destroy]

    # GET /api/orders
    def index
      @orders = Order.all

      render json: @orders
    end

    # GET /api/orders/1
    def show
      render json: @order
    end

    # POST /api/orders
    def create
      @order = Order.new(order_params)

      if @order.save
        render json: @order, status: :created, location: api_order_url(@order)
      else
        render json: @order.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/orders/1
    def update
      if @order.present?
        if @order.update(order_params)
          render json: @order
        else
          render json: @order.errors, status: :unprocessable_entity
        end
      end
    end

    # DELETE /api/orders/1
    def destroy
      @order.destroy if @order.present?
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error{ 'Order record is not found' }
      nil
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:discount, :brute, :net, :payed, :payed_at, :customer_id, :waiter_id, product_ids: [], table_ids: [])
    end
  end
end
