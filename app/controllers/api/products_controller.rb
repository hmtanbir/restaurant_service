module Api
  class ProductsController < ApiController
    before_action :set_product, only: [:show, :update, :destroy]

    # GET /api/products
    def index
      @products = Product.all

      render json: @products
    end

    # GET /api/products/1
    def show
      render json: @product
    end

    # POST /api/products
    def create
      @product = Product.new(product_params)

      if @product.save
        render json: @product, status: :created, location: api_product_url(@product)
      else
        render json: @product.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/products/1
    def update
      if @product.present?
        if @product.update(product_params)
          render json: @product
        else
          render json: @product.errors, status: :unprocessable_entity
        end
      end
    end

    # DELETE /api/products/1
    def destroy
      @product.destroy if @product.present?
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error{ 'Product record is not found' }
      nil
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:name, :price, :description, :category_id)
    end
  end
end
